import Logger from "js-logger";

import {AgentInterface} from "./Agents";
import {Order} from "../classes/Order";
import {SuperBuyError} from "../exceptions/SuperBuyError";
import {BuildTaoCarts} from "../helpers/BuildTaoCarts";
import {post} from "../helpers/Fetch";

export class SuperBuy implements AgentInterface {
  private _builder: BuildTaoCarts;

  constructor() {
    this._builder = new BuildTaoCarts();
  }

  get name(): string {
    return "SuperBuy";
  }

  async send(order: Order): Promise<void> {
    // Build the purchase data
    const purchaseData = this._builder.purchaseData(order);

    Logger.info("Sending order to SuperBuy...", purchaseData);

    // Do the actual call
    const response = await post("https://front.superbuy.com/cart/add-cart", {
      body: JSON.stringify(purchaseData),
      headers: {
        origin: "https://www.superbuy.com",
        referer: "https://www.superbuy.com/",
        "content-type": "application/json;charset=UTF-8",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36",
      },
    })

    const data = await response.json();
    if (data.state === 0 && data.msg === "Success") {
      return;
    }

    Logger.error("Item could not be added", data.msg);
    throw new SuperBuyError("Item could not be added");
  }
}
