import Logger from "js-logger";
import {serialize} from "object-to-formdata";

import {AgentInterface} from "./Agents";
import {Order} from "../classes/Order";
import {CSSBuyError} from "../exceptions/CSSBuyError";
import {post} from "../helpers/Fetch";

export class CSSBuy implements AgentInterface {
  get name(): string {
    return "CSSBuy";
  }

  async send(order: Order): Promise<void> {
    // Build the purchase data
    const purchaseData = this._buildPurchaseData(order);

    Logger.info("Sending order to CSSBuy...", purchaseData);

    // Do the actual call
    const response = await post("https://www.cssbuy.com/ajax/fast_ajax.php?action=buyone", {
      body: new URLSearchParams(serialize(purchaseData) as any),
      referrer: `https://www.cssbuy.com/?go=item&url=${encodeURIComponent(purchaseData.data.href)}`,
      referrerPolicy: "strict-origin-when-cross-origin",
      headers: {
        "accept": "application/json, text/javascript, */*; q=0.01",
        "accept-language": "nl,en-US;q=0.9,en;q=0.8,de;q=0.7,und;q=0.6",
        "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "referrer": `https://www.cssbuy.com/?go=item&url=${encodeURIComponent(purchaseData.data.href)}`,
        "referrerPolicy": "strict-origin-when-cross-origin",
        "x-requested-with": "XMLHttpRequest"
      },
    })

    if ((await response.json()).ret === 0) {
      return;
    }

    Logger.error("Item could not be added", response);
    throw new CSSBuyError("Item could not be added");
  }

  private _buildPurchaseData(order: Order) {
    // Build the description
    const description = this._buildRemark(order);

    // Create the purchasing data
    return {
      data: {
        buynum: 1,
        shopid: order.shop.id,
        picture: order.item.imageUrl,
        defaultimg: order.item.imageUrl,
        freight: order.shipping,
        price: order.price,
        color: order.item.color,
        size: order.item.size,
        total: order.price + order.shipping,
        buyyourself: 0,
        seller: order.shop.name,
        href: window.location.href,
        title: order.item.name,
        note: description,
        option: description,
      },
    };
  }

  private _buildRemark(order: Order) {
    const descriptionParts: string[] = [];
    if (order.item.model !== null) descriptionParts.push(`Model: ${order.item.model}`);
    if (order.item.color !== null) descriptionParts.push(`Color: ${order.item.color}`);
    if (order.item.size !== null) descriptionParts.push(`Size: ${order.item.size}`);
    if (order.item.other.length !== 0) descriptionParts.push(`${order.item.other}`);

    let description = null;
    if (descriptionParts.length !== 0) {
      description = descriptionParts.join(" / ");
    }

    return description;
  }
}
