import {PandaBuyLogin} from "./Pandabuy";

export interface LoginInterface {
  supports(hostname: string): boolean;

  process(): void;
}

export default function getLogin(hostname: string): LoginInterface|null {
  const agents = [new PandaBuyLogin()];

  let agent = null;
  Object.values(agents).forEach((value: LoginInterface) => {
    if (value.supports(hostname)) {
      agent = value;
    }
  });

  return agent;
}
