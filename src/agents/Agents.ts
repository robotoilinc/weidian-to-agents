import { BaseTao } from "./BaseTao";
import { CSSBuy } from "./CSSBuy";
import { PandaBuy } from "./PandaBuy";
import { SuperBuy } from "./SuperBuy";
import { WeGoBuy } from "./WeGoBuy";
import {Order} from "../classes/Order";

export interface AgentInterface {
  get name(): string;

  send(order: Order): Promise<void>;
}

export const getAgent = (selection: string): AgentInterface => {
  switch (selection) {
    case "basetao":
      return new BaseTao();
    case "cssbuy":
      return new CSSBuy();
    case "pandabuy":
      return new PandaBuy();
    case "superbuy":
      return new SuperBuy();
    case "wegobuy":
      return new WeGoBuy();
    default:
      throw new Error(`Agent '${selection}' is not implemented`);
  }
};
