import GMStorage from "gm-storage";

import {Store} from "./Stores";
import {getAgent} from "../agents/Agents";
import {Item} from "../classes/Item";
import {Order} from "../classes/Order";
import {Shop} from "../classes/Shop";
import {elementReady} from "../helpers/ElementReady";
import {removeWhitespaces} from "../helpers/RemoveWhitespaces";
import {Snackbar} from "../helpers/Snackbar";

export class Store1688 implements Store {
  private store: GMStorage;

  constructor() {
    this.store = new GMStorage();
  }

  public attach($document: JQuery<Document>, localWindow: Window): void {
    elementReady(".order-button-wrapper > .order-button-children > .order-button-children-list").then((element) => {
      const button = this._buildButton($document, localWindow);
      if (button === null) {
        return;
      }

      $(element).prepend(button);
    });
  }

  public supports(hostname: string): boolean {
    return hostname.includes("1688.com");
  }

  private _buildButton($document: JQuery<Document>, window: Window): JQuery|null {
    // Force someone to select an agent
    if (this.store.get("agentSelection") === "empty") {
      GM_config.open();

      Snackbar("Please select what agent you use");
      return null;
    }

    // Get the agent related to our config
    const agent = getAgent(this.store.get("agentSelection", "") as string);

    // Create button
    const $button = $(`<button id="agent-button" class="order-normal-button order-button">Add to ${agent.name}</button>`);

    $button.on("click", async () => {
      // Disable button to prevent double clicks and show clear message
      $button.attr("disabled", "disabled").text("Processing...");

      // Try to build and send the order
      try {
        await agent.send(this._buildOrder($document, window));
      } catch (err: any) {
        $button.attr("disabled", null).text(`Add to ${agent.name}`);
        return Snackbar(err);
      }

      $button.attr("disabled", null).text(`Add to ${agent.name}`);

      // Success, tell the user
      return Snackbar("Item has been added, be sure to double check it");
    });

    return $('<div class="order-button-tip-wrapper"></div>').append($button);
  }

  private _buildShop(window: Window | any): Shop {
    const id = window.__GLOBAL_DATA.offerBaseInfo.sellerUserId;
    const name = window.__GLOBAL_DATA.offerBaseInfo.sellerLoginId;
    const url = new URL(window.__GLOBAL_DATA.offerBaseInfo.sellerWinportUrl, window.location).toString();

    return new Shop(id, name, url);
  }

  private _buildItem($document: JQuery<Document>, window: Window | any): Item {
    // Build item information
    const id = window.__GLOBAL_DATA.tempModel.offerId;
    const name = removeWhitespaces(window.__GLOBAL_DATA.tempModel.offerTitle);

    // Build image information
    const imageUrl = new URL(window.__GLOBAL_DATA.images[0].size310x310ImageURI, window.location).toString();

    // Retrieve the dynamic selected item
    const skus = this._processSku($document);

    return new Item(id, name, imageUrl, null, null, null, skus);
  }

  private _buildPrice($document: JQuery<Document>): number {
    const itemPrice = Number(removeWhitespaces($document.find(".order-price-wrapper .total-price .value").text()));
    if (Number.isNaN(itemPrice)) {
      return 0;
    }

    return itemPrice;
  }

  private _buildShipping($document: JQuery<Document>): number {
    const shippingPrice = Number(removeWhitespaces($document.find(".logistics-express .logistics-express-price").text()));
    if (Number.isNaN(shippingPrice)) {
      return 0;
    }

    return shippingPrice;
  }

  private _buildOrder($document: JQuery<Document>, window: Window) {
    return new Order(this._buildShop(window), this._buildItem($document, window), this._buildPrice($document), this._buildShipping($document));
  }

  private _processSku($document: JQuery<Document>): string[] {
    const selectedItems: string[] = [];

    // Grab the module that holds the selected data
    const skuData = this._findModule($document.find(".pc-sku-wrapper")[0]).getSkuData();

    // Grab the map we can use to find names
    const skuMap = skuData.skuState.skuSpecIdMap;

    // Parse all the selected items
    const selectedData = skuData.skuPannelInfo.getSubmitData().submitData;

    // Ensure at least one item is selected
    if (typeof selectedData.find((item: { specId: any }) => item.specId !== null) === "undefined") {
      throw new Error("Make sure to select at least one item");
    }

    // Process all selections
    selectedData.forEach((item: { specId: string | number; quantity: any }) => {
      const sku = skuMap[item.specId];

      // Build the proper name
      let name = removeWhitespaces(sku.firstProp);
      if (sku.secondProp != null && sku.secondProp.length !== 0) {
        name = `${name} - ${removeWhitespaces(sku.secondProp)}`;
      }

      // Add it to the list with quantity
      selectedItems.push(`${name}: ${item.quantity}x`);
    });

    return selectedItems;
  }

  private _findModule($element: JQuery<Document> | any): any {
    const instanceKey = Object.keys($element).find((key) => key.startsWith("__reactInternalInstance$")) as string;
    const internalInstance = $element[instanceKey];
    if (internalInstance == null) return null;

    return internalInstance.return.ref.current;
  }
}
