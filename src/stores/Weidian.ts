import {Store} from "./Stores";
import {getAgent} from "../agents/Agents";
import {Item} from "../classes/Item";
import {Order} from "../classes/Order";
import {Shop} from "../classes/Shop";
import {elementReady} from "../helpers/ElementReady";
import {removeWhitespaces} from "../helpers/RemoveWhitespaces";
import {retrieveDynamicInformation} from "../helpers/RetrieveDynamicInformation";
import {Snackbar} from "../helpers/Snackbar";

export class Weidian implements Store {
  public attach($document: JQuery<Document>, localWindow: Window | any): void {
    $document.find(".footer-btn-container > span").add(".item-container > .sku-button").on("click", () => {
      // Force someone to select an agent
      if (GM_config.get("agentSelection") === "empty") {
        alert("Please select what agent you use");
        GM_config.open();

        return;
      }

      this._attachFooter($document, localWindow);
      this._attachFooterBuyNow($document, localWindow);
    });

    // Setup for storefront
    $document.on("mousedown", "div.base-ct.img-wrapper", () => {
      // Force new tab for shopping cart (must be done using actual window and by overwriting window.API.Bus)
      localWindow.API.Bus.on("onActiveSku", ((t: number) => localWindow.open(`https://weidian.com/item.html?itemID=${t}&frb=open`).focus()));
    });

    // Check if we are a focused screen (because of storefront handler) and open the cart right away
    if (new URLSearchParams(localWindow.location.search).get("frb") === "open") {
      $document.find("[data-spider-action-name='add_Cart']").trigger("click");
    }
  }

  public supports(hostname: string): boolean {
    return hostname.includes("weidian.com");
  }

  private _attachFooter($document: JQuery<Document>, window: Window): void {
    // Attach button the footer (buy with options or cart)
    elementReady(".sku-footer").then((element) => {
      const $element = $(element);

      // Only add the button if it doesn't exist
      if ($element.parent().find("#agent-button").length !== 0) {
        return;
      }

      // Add the agent button, if we have one
      const button = this._attachButton($document, window);
      if (button === null) {
        return;
      }

      $element.after(button);
    });
  }

  private _attachFooterBuyNow($document: JQuery<Document>, window: Window): void {
    // Attach button the footer (buy now)
    elementReady("#login_quickLogin_wrapper").then((element) => {
      const $parent = $(element).parent();

      // Only add the button if it doesn't exist
      if ($parent.parent().find("#agent-button").length !== 0) {
        return;
      }

      // Add the agent button, if we have one
      const button = this._attachButton($document, window);
      if (button === null) {
        return;
      }

      $parent.after(button);
    });
  }

  private _attachButton($document: JQuery<Document>, window: Window): JQuery | null {
    // Force someone to select an agent
    if (GM_config.get("agentSelection") === "empty") {
      GM_config.open();

      Snackbar("Please select what agent you use");
      return null;
    }

    // Get the agent related to our config
    const agent = getAgent(GM_config.get("agentSelection"));

    const $button = $(`<button id="agent-button">Add to ${agent.name}</button>`)
      .css("background", "#f29800")
      .css("color", "#FFFFFF")
      .css("font-size", "15px")
      .css("text-align", "center")
      .css("padding", "15px 0")
      .css("width", "100%")
      .css("height", "100%")
      .css("cursor", "pointer");

    $button.on("click", async () => {
      // Disable button to prevent double clicks and show clear message
      $button.attr("disabled", "disabled").text("Processing...");

      // Try to build and send the order
      try {
        await agent.send(this._buildOrder($document, window));
      } catch (err: any) {
        $button.attr("disabled", null).text(`Add to ${agent.name}`);
        Snackbar(err);
        return;
      }

      $button.attr("disabled", null).text(`Add to ${agent.name}`);

      // Success, tell the user
      Snackbar("Item has been added, be sure to double check it");
      return;
    });

    return $button;
  }

  private _buildShop($document: JQuery<Document>, window: Window): Shop {
    // Setup default values for variables
    let id = null;
    let name = null;
    let url = null;

    // Try and fill the variables
    let $shop = $document.find(".shop-toggle-header-name").first();
    if ($shop.length !== 0) {
      name = removeWhitespaces($shop.text());
    }

    $shop = $document.find(".item-header-logo").first();
    if ($shop.length !== 0) {
      url = new URL($shop.attr("href") as string, window.location.href).toString();
      id = url.replace(/^\D+/g, "");
      name = removeWhitespaces($shop.text());
    }

    $shop = $document.find(".shop-name-str").first();
    if ($shop.length !== 0) {
      url = new URL($shop.parents("a").first().attr("href") as string, window.location.href).toString();
      id = url.replace(/^\D+/g, "");
      name = removeWhitespaces($shop.text());
    }

    // If no shop name is defined, just set shop ID
    if ((name === null || name.length === 0) && id !== null) {
      name = id;
    }

    return new Shop(id, name, url);
  }

  private _buildItem($document: JQuery<Document>, window: Window): Item {
    // Build item information
    const id = (window.location.href.match(/[?&]itemId=(\d+)/i) as Array<string>)[1];
    const name = removeWhitespaces($document.find(".item-title").first().text());

    // Build image information
    let $itemImage = $document.find("img#skuPic");
    if ($itemImage.length === 0) $itemImage = $document.find("img.item-img");
    const imageUrl = $itemImage.first().attr("src") as string;

    const {
      model,
      color,
      size,
      others
    } = retrieveDynamicInformation($document, ".sku-content .sku-row", ".row-title", ".sku-item.selected");

    return new Item(id, name, imageUrl, model, color, size, others);
  }

  private _buildPrice($document: JQuery<Document>): number {
    let $currentPrice = $document.find(".sku-cur-price");
    if ($currentPrice.length === 0) $currentPrice = $document.find(".cur-price");

    return Number(removeWhitespaces($currentPrice.first().text()).replace(/(\D+)/, ""));
  }

  private _buildShipping($document: JQuery<Document>): number {
    const $postageBlock = $document.find(".postage-block").first();
    const postageMatches = removeWhitespaces($postageBlock.text()).match(/([\d.]+)/);

    // If we can't find any numbers, assume free, agents will fix it
    return postageMatches !== null ? Number(postageMatches[0]) : 0;
  }

  private _buildOrder($document: JQuery<Document>, window: Window): Order {
    return new Order(this._buildShop($document, window), this._buildItem($document, window), this._buildPrice($document), this._buildShipping($document));
  }
}
