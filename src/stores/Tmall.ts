import {Store} from "./Stores";
import {getAgent} from "../agents/Agents";
import {Item} from "../classes/Item";
import {Order} from "../classes/Order";
import {Shop} from "../classes/Shop";
import {removeWhitespaces} from "../helpers/RemoveWhitespaces";
import {retrieveDynamicInformation} from "../helpers/RetrieveDynamicInformation";
import {Snackbar} from "../helpers/Snackbar";

export class Tmall implements Store {
  public attach($document: JQuery<Document>, localWindow: Window): void {
    const button = this._buildButton($document, localWindow);
    if (button === null) {
      return;
    }

    $document.find(".tb-btn-basket.tb-btn-sku").before(button);
  }

  public supports(hostname: string): boolean {
    return hostname === "detail.tmall.com";
  }

  private _buildButton($document: JQuery<Document>, window: Window): JQuery|null {
    // Force someone to select an agent
    if (GM_config.get("agentSelection") === "empty") {
      GM_config.open();

      Snackbar("Please select what agent you use");
      return null;
    }

    // Get the agent related to our config
    const agent = getAgent(GM_config.get("agentSelection"));

    const $button = $(`<button id="agent-button">Add to ${agent.name}</button>`)
      .css("width", "180px")
      .css("color", "#FFF")
      .css("border-color", "#F40")
      .css("background", "#F40")
      .css("cursor", "pointer")
      .css("text-align", "center")
      .css("font-family", '"Hiragino Sans GB","microsoft yahei",sans-serif')
      .css("font-size", "16px")
      .css("line-height", "38px")
      .css("border-width", "1px")
      .css("border-style", "solid")
      .css("border-radius", "2px");

    $button.on("click", async () => {
      // Disable button to prevent double clicks and show clear message
      $button.attr("disabled", "disabled").text("Processing...");

      // Try to build and send the order
      try {
        await agent.send(this._buildOrder($document, window));
      } catch (err: any) {
        $button.attr("disabled", null).text(`Add to ${agent.name}`);
        return Snackbar(err);
      }

      $button.attr("disabled", null).text(`Add to ${agent.name}`);

      // Success, tell the user
      return Snackbar("Item has been added, be sure to double check it");
    });

    return $('<div class="tb-btn-add-agent"></div>').append($button);
  }

  private _buildShop(window: Window | any): Shop {
    const id = window.g_config.shopId;
    const name = window.g_config.sellerNickName;
    const url = new URL(window.g_config.shopUrl, window.location).toString();

    return new Shop(id, name, url);
  }

  private _buildItem($document: JQuery<Document>, window: Window | any): Item {
    // Build item information
    const id = window.g_config.itemId;
    const name = removeWhitespaces($document.find("#J_DetailMeta > div.tm-clear > div.tb-property > div > div.tb-detail-hd > h1").text());

    // Build image information
    const imageUrl = $document.find("#J_ImgBooth").first().attr("src") as string;

    // Retrieve the dynamic selected item
    const { model, color, size, others} = retrieveDynamicInformation($document, ".tb-skin > .tb-sku > .tb-prop", ".tb-metatit", ".tb-selected");

    return new Item(id, name, imageUrl, model, color, size, others);
  }

  private _buildPrice($document: JQuery<Document>): number {
    let price = Number(removeWhitespaces($document.find(".tm-price").first().text()));
    $document.find(".tm-price").each((key, element: { textContent: any }) => {
      const currentPrice = Number(removeWhitespaces(element.textContent));
      if (price > currentPrice) price = currentPrice;
    });

    return price;
  }

  private _buildShipping($document: JQuery<Document>): number {
    const postageText = removeWhitespaces($document.find("#J_PostageToggleCont > p > .tm-yen").first().text());

    // Check for free shipping
    if (postageText.includes("快递 免运费")) {
      return 0;
    }

    // Try and get postage from text
    const postageMatches = postageText.match(/([\d.]+)/);

    // If we can't find any numbers, assume free as well, agents will fix it
    return postageMatches !== null ? Number(postageMatches[0]) : 0;
  }

  private _buildOrder($document: JQuery<Document>, window: Window): Order {
    return new Order(this._buildShop(window), this._buildItem($document, window), this._buildPrice($document), this._buildShipping($document));
  }
}
