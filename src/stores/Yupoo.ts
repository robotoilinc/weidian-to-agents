import {Store} from "./Stores";
import {getAgent} from "../agents/Agents";
import {Item} from "../classes/Item";
import {Order} from "../classes/Order";
import {Shop} from "../classes/Shop";
import {removeWhitespaces} from "../helpers/RemoveWhitespaces";
import {Snackbar} from "../helpers/Snackbar";

export class Yupoo implements Store {
  public attach($document: JQuery<Document>, localWindow: Window): void {
    // Setup for item page
    const button = this._buildButton($document, localWindow);
    if (button === null) {
      return;
    }

    $document.find(".showalbumheader__tabgroup").prepend(button);
  }

  public supports(hostname: string): boolean {
    return hostname.includes("yupoo.com");
  }

  private _buildButton($document: JQuery<Document>, window: Window): JQuery | null {
    // Force someone to select an agent
    if (GM_config.get("agentSelection") === "empty") {
      GM_config.open();

      Snackbar("Please select what agent you use");
      return null;
    }

    // Get the agent related to our config
    const agent = getAgent(GM_config.get("agentSelection"));

    const $button = $(`<a id="agent-button" class="button showalbumheader__copy" style="background: rgb(242, 152, 0); color: rgb(255, 255, 255);">Add to ${agent.name}</a>`);
    $button.on("click", async () => {
      // Disable button to prevent double clicks and show clear message
      $button.attr("disabled", "disabled").text("Processing...");

      // Try to build and send the order
      try {
        await agent.send(this._buildOrder($document, window));
      } catch (err: any) {
        $button.attr("disabled", null).text(`Add to ${agent.name}`);
        return Snackbar(err);
      }

      $button.attr("disabled", null).text(`Add to ${agent.name}`);

      // Success, tell the user
      return Snackbar("Item has been added, be sure to double check it");
    });

    return $button;
  }

  private _buildShop($document: JQuery<Document>, window: Window): Shop {
    // Setup default values for variables
    const author = window.location.hostname.replace(".x.yupoo.com", "");
    const name = $document.find(".showheader__headerTop > h1").first().text();
    const url = `https://${author}.x.yupoo.com/albums`;

    return new Shop(author, name, url);
  }

  private _buildItem($document: JQuery<Document>, window: Window): Item {
    // Build item information
    const id = (window.location.href.match(/albums\/(\d+)/i) as Array<string>)[1];
    const name = removeWhitespaces($document.find("h2 > .showalbumheader__gallerytitle").first().text());

    // Build image information
    const $itemImage = $document.find(".showalbumheader__gallerycover > img").first();
    const imageUrl = new URL(($itemImage.attr("src") as string).replace("photo.yupoo.com/", "cdn.fashionreps.page/yupoo/"), window.location.href).toString();

    // Ask for dynamic information
    let color = prompt("What color (leave blank if not needed)?");
    if (color !== null && removeWhitespaces(color).length === 0) {
      color = null
    }

    let size = prompt("What size (leave blank if not needed)?");
    if (size !== null && removeWhitespaces(size).length === 0) {
      size = null
    }

    return new Item(id, name, imageUrl, null, color, size, []);
  }

  private _buildPrice($document: JQuery<Document>): number {
    const priceHolder = $document.find("h2 > .showalbumheader__gallerytitle");
    let currentPrice = "0";

    // Try and find the price of the item
    const priceMatcher = priceHolder.text().match(/¥?(\d+)¥?/i);
    if (priceHolder && priceMatcher && priceMatcher.length !== 0) {
      currentPrice = priceMatcher[1];
    }

    const predeterminedPrice = Number(removeWhitespaces(currentPrice).replace(/(\D+)/, ""));
    const price = prompt("How much is the item?", String(predeterminedPrice));
    if (price === null || removeWhitespaces(price).length === 0) {
      return predeterminedPrice;
    }

    return Number(removeWhitespaces(price))
  }

  private _buildOrder($document: JQuery<Document>, window: Window): Order {
    return new Order(this._buildShop($document, window), this._buildItem($document, window), this._buildPrice($document), 10);
  }
}
