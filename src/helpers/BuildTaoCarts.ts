import {Order} from "../classes/Order";

export class BuildTaoCarts {
  public purchaseData(order: Order): object {
    // Build the description
    const description = this._buildRemark(order);

    // Generate an SKU based on the description
    let sku = null;
    if (description !== null && description.length !== 0) {
      sku = description.split("").reduce((a, b) => (((a << 5) - a) + b.charCodeAt(0)) | 0, 0);
    }

    // Create the purchasing data
    return {
      type: 1,
      shopItems: [{
        shopLink: "",
        shopSource: "NOCRAWLER",
        shopNick: "",
        shopId: "",
        goodsItems: [{
          beginCount: 0,
          count: 1,
          desc: description,
          freight: order.shipping,
          freightServiceCharge: 0,
          goodsAddTime: Math.floor(Date.now() / 1000),
          goodsCode: `NOCRAWLER-${sku}`,
          goodsId: window.location.href,
          goodsLink: window.location.href,
          goodsName: order.item.name,
          goodsPrifex: "NOCRAWLER",
          goodsRemark: description,
          guideGoodsId: "",
          is1111Yushou: "no",
          picture: order.item.imageUrl,
          platForm: "pc",
          price: order.price,
          priceNote: "",
          serviceCharge: 0,
          sku: order.item.imageUrl,
          spm: "",
          warehouseId: "1",
        }],
      }],
    };
  }

  private _buildRemark(order: Order): string | null {
    const descriptionParts: string[] = [];
    if (order.item.model !== null) descriptionParts.push(`Model: ${order.item.model}`);
    if (order.item.color !== null) descriptionParts.push(`Color: ${order.item.color}`);
    if (order.item.size !== null) descriptionParts.push(`Size: ${order.item.size}`);
    if (order.item.other.length !== 0) descriptionParts.push(`${order.item.other}`);

    let description = null;
    if (descriptionParts.length !== 0) {
      description = descriptionParts.join(" / ");
    }

    return description;
  }
}
