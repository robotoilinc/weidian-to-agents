/**
 * Removes all emojis from the input text.
 *
 * @param string {string}
 */
export const removeEmoji = (string: string) => string.replace(/[^\p{L}\p{N}\p{P}\p{Z}^$\n]/gu, "");
