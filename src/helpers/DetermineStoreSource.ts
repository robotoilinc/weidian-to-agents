/**
 * Determines on website we are buying something. Used for BaseTao and Pandabuy.
 */
export const determineStoreSource = function(): string {
  if (window.location.hostname.includes("1688.com")) {
    return "1688";
  }

  // Check more specific TaoBao pages first
  if (window.location.hostname.includes("market.m.taobao.com") || window.location.hostname.includes("2.taobao.com")) {
    return "xianyu";
  }

  if (window.location.hostname.includes("taobao.com")) {
    return "taobao";
  }

  if (window.location.hostname.includes("weidian.com") || window.location.hostname.includes("koudai.com")) {
    return "wd";
  }

  if (window.location.hostname.includes("yupoo.com")) {
    return "yupoo";
  }

  if (window.location.hostname.includes("detail.tmall.com")) {
    return "tmall";
  }

  throw new Error(`Could not determine store source ${window.location.hostname}`);
}
