/**
 * Trims the input text and removes all in between spaces as well.
 *
 * @param string {string}
 */
export const removeWhitespaces = (string: string) => string.trim().replace(/\s(?=\s)/g, "");
