import Logger from "js-logger";

export const Snackbar = function (toast: string) {
    // Log the snackbar, for ease of debugging
    Logger.info(toast);

    // Setup toast element
    const $toast = $(`<div style="background-color:#333;border-radius:2px;bottom:50%;color:#fff;display:block;font-size:16px;left:50%;margin-left:-150px;min-width:250px;opacity:1;padding:16px;position:fixed;right:50%;text-align:center;transition:background .2s;width:300px;z-index:2147483647">${toast}</div>`);

    // Append to the body
    $("body").append($toast);

    // Set a timeout to remove the toast
    setTimeout(() => $toast.fadeOut("slow", () => $toast.remove()), 5000);
};
