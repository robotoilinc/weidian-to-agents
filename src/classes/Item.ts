export class Item {
  public readonly id: string;
  public readonly name: string;
  public readonly imageUrl: string;
  public readonly model: string|null;
  public readonly color: string|null;
  public readonly size: string|null;
  public readonly others: string[];

  constructor(id: string, name: string, imageUrl: string, model: string|null, color: string|null, size: string|null, others: string[]) {
    this.id = id;
    this.name = name;
    this.imageUrl = imageUrl;
    this.model = model;
    this.color = color;
    this.size = size;
    this.others = others;
  }

  get other(): string {
    return this.others.join("\n");
  }
}
