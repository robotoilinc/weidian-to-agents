export class Shop {
  public readonly id: string|null;
  public readonly name: string|null;
  public readonly url: string|null;

  constructor(id: string|null, name: string|null, url: string|null) {
    this.id = id;
    this.name = name;
    this.url = url;
  }
}
