export class WeGoBuyError extends Error {
  constructor(message: string) {
    super(message);
    this.name = "WeGoBuyError";
  }
}
