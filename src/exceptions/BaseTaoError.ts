export class BaseTaoError extends Error {
  constructor(message: string) {
    super(message);
    this.name = "BaseTaoError";
  }
}
