export class SuperBuyError extends Error {
  constructor(message: string) {
    super(message);
    this.name = "SuperBuyError";
  }
}
