export class CSSBuyError extends Error {
  constructor(message: string) {
    super(message);
    this.name = "CSSBuyError";
  }
}
