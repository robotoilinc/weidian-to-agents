const webpack = require('webpack');
const ESLintPlugin = require('eslint-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const WebpackBuildNotifierPlugin = require('webpack-build-notifier');

const pkg = require('../package.json');

const webpackConfig = {
  resolve: {
    extensions: ['.js', '.ts']
  },
  optimization: {
    minimize: false,
    moduleIds: 'named',
  },
  entry: './src/index.ts',
  target: 'web',
  externals: {
    jquery: '$',
    axios: 'axios',
    'axios-userscript-adapter': 'axiosGmxhrAdapter',
    'js-logger': 'Logger',
    'gm-storage': 'GMStorage',
  },
  module: {
    rules: [
      {
        use: {
          loader: 'babel-loader',
        },
        test: /\.js$/,
      },
      {
        test: /\.ts$/,
        loader: 'babel-loader',
      },
      {
        test: /\.less$/,
        use: [
          'style-loader',
          'css-loader',
          'less-loader',
        ]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ]
      }
    ]
  },
  plugins: [
    new webpack.AutomaticPrefetchPlugin(),
    new webpack.BannerPlugin({ banner: `${pkg.name} v${pkg.version} has been created by ${pkg.author.name}. All rights reserved.`, }),
    new ESLintPlugin({
      extensions: ['js', 'ts'],
      formatter: 'compact',
      fix: true,
    }),
    new ForkTsCheckerWebpackPlugin({
      typescript: {
        diagnosticOptions: {
          semantic: true,
          syntactic: true,
        },
        mode: 'write-references',
      },
    }),
    new WebpackBuildNotifierPlugin({
      title: `${pkg.name}`,
      suppressSuccess: true,
    })
  ]
};

module.exports = webpackConfig;
