const pkg = require('../package.json');

module.exports = {
  name: 'Stores to Agent',
  namespace: 'https://www.reddit.com/user/RobotOilInc',
  version: pkg.version,
  author: pkg.author.name,
  source: pkg.repository.url,
  license: pkg.license,
  require: [
    `https://unpkg.com/@trim21/gm-fetch@${pkg.dependencies['@trim21/gm-fetch']}/dist/gm_fetch.js#sha384-P0KSVCS+YC1OEOII7FniE0zZ0+xpXAOHHT5aY++HbYNTnJbp8R933m5CVq4XJv4O`,
    `https://unpkg.com/gm-storage@${pkg.dependencies['gm-storage']}/dist/index.umd.min.js#sha384-IforrfCAVNj1KYlzIXNAyX2RADvDkkufuRfcM0qT57QMNdEonuvBRK2WfL8Co3JV`,
    `https://unpkg.com/gm4-polyfill@${pkg.dependencies['gm4-polyfill']}/gm4-polyfill.js#sha384-nDSbBzN1Jn1VgjQjt5u2BxaPO1pbMS9Gxyi5+yIsKYWzqkYOEh11iQdomqywYPaN`,
    `https://unpkg.com/jquery@${pkg.dependencies.jquery}/dist/jquery.min.js#sha384-NXgwF8Kv9SSAr+jemKKcbvQsz+teULH/a5UNJvZc6kP47hZgl62M1vGnw6gHQhb1`,
    `https://unpkg.com/js-logger@${pkg.dependencies['js-logger']}/src/logger.min.js#sha384-CGmI56C3Kvs2e+Ftr3UpFkkMgOAXBUkLKS/KVkxDEuGSUYF8qki7CzwWWBz4QM60`,
    'https://greasyfork.org/scripts/11562-gm-config-8/code/GM_config%208+.js?version=66657#sha256-229668ef83cd26ac207e9d780e2bba6658e1506ac0b23fb29dc94ae531dd31fb'
  ],
  description: pkg.description,
  homepageURL: 'https://greasyfork.org/en/scripts/427774-stores-to-agent',
  supportURL: 'https://greasyfork.org/en/scripts/427774-stores-to-agent',
  downloadURL: 'https://greasyfork.org/scripts/427774-stores-to-agent/code/Stores%20to%20Agent.user.js',
  updateURL: 'https://greasyfork.org/scripts/427774-stores-to-agent/code/Stores%20to%20Agent.meta.js',
  match: [
    'https://detail.1688.com/offer/*',
    'https://*.taobao.com/item.htm*',
    'https://*.v.weidian.com/?userid=*',
    'https://*.weidian.com/item.html*',
    'https://*.yupoo.com/albums/*',
    'https://detail.tmall.com/item.htm*',
    'https://weidian.com/*itemID=*',
    'https://weidian.com/?userid=*',
    'https://weidian.com/item.html*',
    'https://*.pandabuy.com/*',
    'https://www.pandabuy.com/*'
  ],
  grant: [
    'GM_addStyle',
    'GM_getResourceText',
    'GM_getValue',
    'GM_setValue',
    'GM_registerMenuCommand',
    'GM_webRequest',
    'GM_xmlhttpRequest',
    'GM_deleteValue',
    'GM_listValues'
  ],
  connect: [
    'basetao.com',
    'cssbuy.com',
    'superbuy.com',
    'ytaopal.com',
    'wegobuy.com',
    'pandabuy.com'
  ],
  webRequest: '[{ "selector": "*thor.weidian.com/stardust/*", "action": "cancel" }]',
  icon: 'https://i.imgur.com/2lQXuqv.png',
  'run-at': 'document-end'
};
